package com.likandr.weather20.common.base.presenter;

import com.likandr.weather20.common.base.BaseMvpView;

import io.reactivex.disposables.Disposable;

public interface IBasePresenter<V extends BaseMvpView> {

    void attachView(V view);
    void detachView();
    boolean isViewAttached();
    void checkViewAttached();
}
