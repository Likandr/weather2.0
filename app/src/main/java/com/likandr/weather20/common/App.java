package com.likandr.weather20.common;

import android.app.Application;

import com.likandr.weather20.BuildConfig;
import com.likandr.weather20.common.db.RoomModule;
import com.likandr.weather20.common.net.NetModule;
import com.likandr.weather20.common._di.AppComponent;
import com.likandr.weather20.common._di.AppModule;
import com.likandr.weather20.common._di.DaggerAppComponent;
import com.squareup.leakcanary.LeakCanary;

public class App extends Application {

    public static final String TAG = App.class.getSimpleName();

    private static App sInstance;
    private AppComponent component;

    public AppComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        if (BuildConfig.DEBUG) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                return;
            }
            LeakCanary.install(this);
        }

        this.setAppComponent();
    }

    private void setAppComponent() {
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(sInstance))
                .roomModule(new RoomModule(sInstance))
                .netModule(new NetModule())
                .build();
    }

    public static App get() {
        return sInstance;
    }
}