package com.likandr.weather20.common.net;

public class NetConst {
    public static final String API_BASE_URL = "https://api.openweathermap.org/";
    private static final String APPID = "cd108bd5460fa6bf11a52a6e21cd3ad2";

    public static final String CITY_DATA = "data/2.5/group?units=metric&appid=" + APPID;
    public static final String CITY_DETAIL = "data/2.5/forecast/daily?&mode=json&units=metric&appid=" + APPID;
}
