package com.likandr.weather20.common.base.converters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class BaseTypeConverter<TypeData> {

    private Gson gson = new Gson();
    private Type type = new TypeToken<TypeData>(){}.getType();

    @TypeConverter public TypeData fromString(String json) {
        return gson.fromJson(json, type);
    }

    @TypeConverter public String toString(TypeData cityEntities) {
        return gson.toJson(cityEntities, type);
    }
}