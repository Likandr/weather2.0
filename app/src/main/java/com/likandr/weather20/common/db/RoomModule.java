package com.likandr.weather20.common.db;

import android.arch.persistence.room.Room;

import com.likandr.weather20.common.App;
import com.likandr.weather20.common._di.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    private MyDatabase myDatabase;

    public RoomModule(App app) {
        this.myDatabase = Room.databaseBuilder(app, MyDatabase.class, "weather-db").build();
    }

    @Provides @ApplicationScope
    MyDatabase providesRoomDatabase() {
        return myDatabase;
    }
}