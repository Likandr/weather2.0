package com.likandr.weather20.common._di;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.likandr.weather20.common.App;
import com.likandr.weather20.common.db.MyDatabase;
import com.likandr.weather20.common.db.RoomModule;
import com.likandr.weather20.common.net.ApiInterface;
import com.likandr.weather20.common.net.NetModule;

import dagger.Component;

@ApplicationScope
@Component(modules = {AppModule.class, NetModule.class, RoomModule.class})
public interface AppComponent {

    void inject(App app);

    Context context();
    App app();
    Gson gson();
//    Cache cache();
//    OkHttpClient okHttpClient();
    ApiInterface apiInterface();
    SharedPreferences sharedPreferences();
    MyDatabase myDatabase();
}
