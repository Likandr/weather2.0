package com.likandr.weather20.common.net;

import com.likandr.weather20.data.city.remote.model.CitiesModel;
import com.likandr.weather20.data.city_detail.remote.entities.CityDetailModel;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET(NetConst.CITY_DATA)
    Single<CitiesModel> getCurrentWeather(
            @Query("id") String idsOfCity,
            @Query("lang") String lang);

    @GET(NetConst.CITY_DETAIL)
    Single<CityDetailModel> getForecast(
            @Query("id") Integer idOfCity,
            @Query("lang") String lang,
            @Query("cnt") Integer countDays);
}
