package com.likandr.weather20.common.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.likandr.weather20.data._model.converters.CloudsTypeConverter;
import com.likandr.weather20.data._model.converters.CoordTypeConverter;
import com.likandr.weather20.data._model.converters.MainTypeConverter;
import com.likandr.weather20.data._model.converters.SysTypeConverter;
import com.likandr.weather20.data._model.converters.WeatherTypeConverter;
import com.likandr.weather20.data._model.converters.WindTypeConverter;
import com.likandr.weather20.data.city.local.entities.CityEntity;
import com.likandr.weather20.data.city.local.repositories.CityDao;
import com.likandr.weather20.data.city_detail.local.entities.CityDetailEntity;
import com.likandr.weather20.data.city_detail.local.entities.DailyForecastTypeConverter;
import com.likandr.weather20.data.city_detail.local.repositories.CityDetailDao;

@Database(entities = {CityEntity.class, CityDetailEntity.class},
        version = MyDatabase.VERSION, exportSchema = false)
@TypeConverters({
        CloudsTypeConverter.class,
        CoordTypeConverter.class,
        MainTypeConverter.class,
        SysTypeConverter.class,
        WeatherTypeConverter.class,
        WindTypeConverter.class,
        DailyForecastTypeConverter.class})
public abstract class MyDatabase extends RoomDatabase {

    static final int VERSION = 1;

    public abstract CityDao getCityDao();
    public abstract CityDetailDao getCityDetailDao();
}