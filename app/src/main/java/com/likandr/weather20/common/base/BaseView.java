package com.likandr.weather20.common.base;

import android.content.Context;

public interface BaseView {
    void injectDependencies();

    void attachToPresenter();
    void detachFromPresenter();

    void showLoading();
    void hideLoading();

    void showMessage(String message);
    void showNoNetwork();

    Context getContext();
}
