package com.likandr.weather20.common.base.presenter;

import com.likandr.weather20.common.base.BaseMvpView;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.SingleTransformer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class BasePresenter<T extends BaseMvpView> implements IBasePresenter<T> {

    public T view;

    @Override public void attachView(T view) {
        this.view = view;
    }

    @Override public void detachView() {
        this.view = null;
    }

    @Override public boolean isViewAttached() {
        return view != null;
    }

    @Override public void checkViewAttached() throws ViewNotAttachedException {
        if (!isViewAttached()) throw new ViewNotAttachedException();
    }

    public static class ViewNotAttachedException extends RuntimeException {
        public ViewNotAttachedException() {
            super("Call Presenter.attachView(BaseView) before asking for data");
        }
    }
}