package com.likandr.weather20.data.city;

import com.likandr.weather20.common.base.data.NetworkBoundSource;
import com.likandr.weather20.common.base.misc.Params;
import com.likandr.weather20.data.city.local.entities.CityEntity;
import com.likandr.weather20.data.city.local.repositories.CityDao;
import com.likandr.weather20.data.city.remote.interactors.GetCityUseCase;
import com.likandr.weather20.data.city.remote.model.CitiesModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.functions.Function;

public class CityRepository {

    private GetCityUseCase getCityUseCase;
    private CityDao cityDao;

    @Inject CityRepository(GetCityUseCase getCityUseCase, CityDao cityDao) {
        this.getCityUseCase = getCityUseCase;
        this.cityDao = cityDao;
    }

    public Flowable<List<CityEntity>> getCityList(Params params) {
        return Flowable.create(e -> new NetworkBoundSource<List<CityEntity>, CitiesModel>(e) {
            @Override public Single<CitiesModel> getRemote() {
                return getCityUseCase.getCities(params);
            }

            @Override public Flowable<List<CityEntity>> getLocal() {
                return cityDao.getAll();
            }

            @Override public void saveCallResult(List<CityEntity> data) {
                cityDao.insert(data);
            }

            @Override public Function<CitiesModel, List<CityEntity>> mapper() {
                return CityMapper.transform();
            }
        }, BackpressureStrategy.BUFFER);
    }

    public Flowable<List<CityEntity>> getCityListNoConnect() {
        return cityDao.getAll();
    }

    public Single<List<Integer>> getCityIds() {
        return cityDao.getCityIds();
    }

    public Single<Integer> getCityCount() {
        return cityDao.count();
    }
}
