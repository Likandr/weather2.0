package com.likandr.weather20.data.city_detail.local.entities;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.likandr.weather20.data._model.DailyForecast;

import java.lang.reflect.Type;
import java.util.List;

public class DailyForecastTypeConverter {

    private Gson gson = new Gson();
    private Type type = new TypeToken<List<DailyForecast>>(){}.getType();

    @TypeConverter public List<DailyForecast> fromString(String json) {
        return gson.fromJson(json, type);
    }

    @TypeConverter public String toString(List<DailyForecast> dailyForecast) {
        return gson.toJson(dailyForecast, type);
    }
}