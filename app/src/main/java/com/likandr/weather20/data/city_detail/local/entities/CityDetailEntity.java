package com.likandr.weather20.data.city_detail.local.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.likandr.weather20.data._model.DailyForecast;

import java.util.List;

import static com.likandr.weather20.data.city_detail.local.entities.CityDetailEntity.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class CityDetailEntity {
    public static final String TABLE_NAME = "city_detail";

    @Expose
    @PrimaryKey
    @SerializedName("id")
    private Integer id;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("cnt")
    private Integer cnt;
    @Expose
    @SerializedName("list")
    @TypeConverters(DailyForecastTypeConverter.class)
    private List<DailyForecast> list = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public List<DailyForecast> getList() {
        return list;
    }

    public void setList(List<DailyForecast> list) {
        this.list = list;
    }
}
