package com.likandr.weather20.data._model.converters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.likandr.weather20.data._model.Weather;

import java.lang.reflect.Type;
import java.util.List;

public class WeatherTypeConverter {

    private Gson gson = new Gson();
    private Type type = new TypeToken<List<Weather>>(){}.getType();

    @TypeConverter public List<Weather> fromString(String json) {
        return gson.fromJson(json, type);
    }

    @TypeConverter public String toString(List<Weather> cityEntities) {
        return gson.toJson(cityEntities, type);
    }
}
