package com.likandr.weather20.data.city.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.likandr.weather20.data.city.local.entities.CityEntity;

import java.util.List;

public class CitiesModel {

    @Expose
    @SerializedName("cnt")
    private Integer cnt;
    @Expose
    @SerializedName("list")
    private List<CityEntity> list = null;

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public List<CityEntity> getList() {
        return list;
    }

    public void setList(List<CityEntity> list) {
        this.list = list;
    }
}