package com.likandr.weather20.data._model;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CityDBModel extends RealmObject implements Copyable<CityDBModel> {

    @PrimaryKey
    @SerializedName("entry_id")
    private String id;

    @SerializedName("entry_result")
    private String result;

    @SerializedName("entry_date")
    private String date;

    public CityDBModel() {
    }

    public CityDBModel(String id, String result, String date) {
        this.id = id;
        this.result = result;
        this.date = date;
    }

    public CityDBModel(String result, String date) {
        this.id = generateId();
        this.result = result;
        this.date = date;
    }

    @Override
    public CityDBModel getCopy() {
        return new CityDBModel(generateId(), getResult(), getDate());
    }

    private String generateId() {
        return date + "_" + UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
