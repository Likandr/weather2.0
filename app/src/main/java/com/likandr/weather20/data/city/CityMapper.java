package com.likandr.weather20.data.city;

import com.likandr.weather20.data.city.local.entities.CityEntity;
import com.likandr.weather20.data.city.remote.model.CitiesModel;

import java.util.List;

import io.reactivex.functions.Function;

public class CityMapper {

    public static Function<CitiesModel, List<CityEntity>> transform() {
        return CitiesModel::getList;
    }
}