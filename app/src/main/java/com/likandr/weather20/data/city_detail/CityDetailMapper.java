package com.likandr.weather20.data.city_detail;

import com.likandr.weather20.data.city_detail.local.entities.CityDetailEntity;
import com.likandr.weather20.data.city_detail.remote.entities.CityDetailModel;

import io.reactivex.functions.Function;

public class CityDetailMapper {

    public static Function<CityDetailModel, CityDetailEntity> transform() {
        return cityDetailModel -> {
            CityDetailEntity cityDetail = null;
            if (cityDetailModel != null) {
                cityDetail = new CityDetailEntity();
                cityDetail.setId(cityDetailModel.getCity().getId());
                cityDetail.setName(cityDetailModel.getCity().getName());
                cityDetail.setCnt(cityDetailModel.getCnt());
                cityDetail.setList(cityDetailModel.getList());
            }
            return cityDetail;
        };
    }
}