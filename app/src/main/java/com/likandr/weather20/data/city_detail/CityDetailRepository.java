package com.likandr.weather20.data.city_detail;

import com.likandr.weather20.common.base.data.NetworkBoundSource;
import com.likandr.weather20.common.base.misc.Params;
import com.likandr.weather20.data.city_detail.local.entities.CityDetailEntity;
import com.likandr.weather20.data.city_detail.local.repositories.CityDetailDao;
import com.likandr.weather20.data.city_detail.remote.entities.CityDetailModel;
import com.likandr.weather20.data.city_detail.remote.interactors.GetDetailsUseCase;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.functions.Function;

public class CityDetailRepository {
    private GetDetailsUseCase getDetailsUseCase;
    private CityDetailDao cityDetailDao;

    @Inject CityDetailRepository(GetDetailsUseCase useCase, CityDetailDao dao) {
        this.getDetailsUseCase = useCase;
        this.cityDetailDao = dao;
    }

    public Flowable<CityDetailEntity> getCityDetail(Params params, Integer i) {
        return Flowable.create(e -> new NetworkBoundSource<CityDetailEntity, CityDetailModel>(e) {
            @Override
            public Single<CityDetailModel> getRemote() {
                return getDetailsUseCase.getForecast(params);
            }

            @Override
            public Flowable<CityDetailEntity> getLocal() {
                return cityDetailDao.findById(i);
            }

            @Override
            public void saveCallResult(CityDetailEntity data) {
                cityDetailDao.insert(data);
            }

            @Override
            public Function<CityDetailModel, CityDetailEntity> mapper() {
                return CityDetailMapper.transform();
            }
        }, BackpressureStrategy.BUFFER);
    }

    public Flowable<String> getCityName(Integer i) {
        return cityDetailDao.getCityName(i);
    }
}