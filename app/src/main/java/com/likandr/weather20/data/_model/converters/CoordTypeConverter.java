package com.likandr.weather20.data._model.converters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.likandr.weather20.data._model.Coord;

import java.lang.reflect.Type;

public class CoordTypeConverter {

    private Gson gson = new Gson();
    private Type type = new TypeToken<Coord>(){}.getType();

    @TypeConverter public Coord fromString(String json) {
        return gson.fromJson(json, type);
    }

    @TypeConverter public String toString(Coord cityEntities) {
        return gson.toJson(cityEntities, type);
    }
}