package com.likandr.weather20.data.city_detail.remote.entities;

import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.likandr.weather20.data._model.City;
import com.likandr.weather20.data._model.DailyForecast;
import com.likandr.weather20.data.city_detail.local.entities.DailyForecastTypeConverter;

import java.util.List;

public class CityDetailModel {
    @Expose
    @SerializedName("city")
    private City city;
    @Expose
    @SerializedName("cod")
    private String cod;
    @Expose
    @SerializedName("message")
    private Double message;
    @Expose
    @SerializedName("cnt")
    private Integer cnt;
    @Expose
    @SerializedName("list")
    @TypeConverters(DailyForecastTypeConverter.class)
    private List<DailyForecast> list = null;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Double getMessage() {
        return message;
    }

    public void setMessage(Double message) {
        this.message = message;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public List<DailyForecast> getList() {
        return list;
    }

    public void setList(List<DailyForecast> list) {
        this.list = list;
    }

}