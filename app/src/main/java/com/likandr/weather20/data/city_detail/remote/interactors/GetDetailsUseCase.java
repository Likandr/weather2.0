package com.likandr.weather20.data.city_detail.remote.interactors;

import com.likandr.weather20.common.base.misc.Params;
import com.likandr.weather20.data.city_detail.remote.entities.CityDetailModel;
import com.likandr.weather20.common.net.ApiInterface;

import javax.inject.Inject;

import io.reactivex.Single;

public class GetDetailsUseCase {

    public final static String PARAMS_KEY_IDS_OF_CITY = "idOfCity";
    public final static String PARAMS_KEY_LANGUAGE = "lang";
    public final static String PARAMS_KEY_COUNT_DAYS = "countDays";

    private ApiInterface api;

    @Inject public GetDetailsUseCase(ApiInterface api) {
        this.api = api;
    }

    public Single<CityDetailModel> getForecast(Params params) {
        return this.api.getForecast(
                params.getInt(PARAMS_KEY_IDS_OF_CITY, 0),
                params.getString(PARAMS_KEY_LANGUAGE, "en"),
                params.getInt(PARAMS_KEY_COUNT_DAYS, 3)
        );
    }
}