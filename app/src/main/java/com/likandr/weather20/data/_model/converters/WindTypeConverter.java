package com.likandr.weather20.data._model.converters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.likandr.weather20.data._model.Wind;

import java.lang.reflect.Type;

public class WindTypeConverter {

    private Gson gson = new Gson();
    private Type type = new TypeToken<Wind>(){}.getType();

    @TypeConverter
    public Wind fromString(String json) {
        return gson.fromJson(json, type);
    }

    @TypeConverter public String toString(Wind cityEntities) {
        return gson.toJson(cityEntities, type);
    }
}
