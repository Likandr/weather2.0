package com.likandr.weather20.data.city.local.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.likandr.weather20.data._model.Clouds;
import com.likandr.weather20.data._model.Coord;
import com.likandr.weather20.data._model.Main;
import com.likandr.weather20.data._model.Sys;
import com.likandr.weather20.data._model.Weather;
import com.likandr.weather20.data._model.Wind;
import com.likandr.weather20.data._model.converters.CloudsTypeConverter;
import com.likandr.weather20.data._model.converters.CoordTypeConverter;
import com.likandr.weather20.data._model.converters.MainTypeConverter;
import com.likandr.weather20.data._model.converters.SysTypeConverter;
import com.likandr.weather20.data._model.converters.WeatherTypeConverter;
import com.likandr.weather20.data._model.converters.WindTypeConverter;

import java.util.List;

import static com.likandr.weather20.data.city.local.entities.CityEntity.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class CityEntity {

    public static final String TABLE_NAME = "city";

    @Expose
    @PrimaryKey
    @SerializedName("id")
    private Integer id;
    @Expose
    @SerializedName("coord")
    @TypeConverters(CoordTypeConverter.class)
    private Coord coord;
    @Expose
    @SerializedName("sys")
    @TypeConverters(SysTypeConverter.class)
    private Sys sys;
    @Expose
    @SerializedName("weather")
    @TypeConverters(WeatherTypeConverter.class)
    private List<Weather> weather = null;
    @Expose
    @SerializedName("main")
    @TypeConverters(MainTypeConverter.class)
    private Main main;
    @Expose
    @SerializedName("wind")
    @TypeConverters(WindTypeConverter.class)
    private Wind wind;
    @Expose
    @SerializedName("clouds")
    @TypeConverters(CloudsTypeConverter.class)
    private Clouds clouds;
    @Expose
    @SerializedName("dt")
    private Integer dt;
    @Expose
    @SerializedName("name")
    private String name;

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Integer getDt() {
        return dt;
    }

    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

