package com.likandr.weather20.data.city.local.repositories;

import com.likandr.weather20.data.city.local.entities.CityEntity;

import java.util.List;

import io.reactivex.Flowable;

public interface ICityDataSource {
    void insert(List<CityEntity> cityEntities);

    void delete(List<CityEntity> cityEntities);

    Flowable<List<CityEntity>> getAll();
}
