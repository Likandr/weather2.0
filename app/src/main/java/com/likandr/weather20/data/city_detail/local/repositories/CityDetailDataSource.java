package com.likandr.weather20.data.city_detail.local.repositories;

import com.likandr.weather20.data.city_detail.local.entities.CityDetailEntity;

import javax.inject.Inject;

import io.reactivex.Single;

public class CityDetailDataSource implements ICityDetailRepository {
    private CityDetailDao cityDetailDao;

    @Inject
    public CityDetailDataSource(CityDetailDao cityDetailDao) {
        this.cityDetailDao = cityDetailDao;
    }

    @Override public long insert(CityDetailEntity cityEntity) {
        return cityDetailDao.insert(cityEntity);
    }

    @Override public void delete(CityDetailEntity cityEntity) {
        cityDetailDao.delete(cityEntity);
    }

    @Override public Single<CityDetailEntity> getAll() {
        return cityDetailDao.getAll();
    }
}
