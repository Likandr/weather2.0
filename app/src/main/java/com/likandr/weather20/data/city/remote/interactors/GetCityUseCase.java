package com.likandr.weather20.data.city.remote.interactors;

import com.likandr.weather20.common.base.misc.Params;
import com.likandr.weather20.common.net.ApiInterface;
import com.likandr.weather20.data.city.remote.model.CitiesModel;

import javax.inject.Inject;

import io.reactivex.Single;

public class GetCityUseCase {

    public final static String PARAMS_KEY_IDS_OF_CITY = "ids_of_city";
    public final static String PARAMS_KEY_LANGUAGE = "language";

    private ApiInterface api;

    @Inject public GetCityUseCase(ApiInterface api) {
        this.api = api;
    }

    public Single<CitiesModel> getCities(Params params) {
        return api.getCurrentWeather(
                params.getString(PARAMS_KEY_IDS_OF_CITY, "524901,519690"),
                params.getString(PARAMS_KEY_LANGUAGE, "en"));
    }
}