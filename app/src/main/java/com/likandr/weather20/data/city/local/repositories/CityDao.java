package com.likandr.weather20.data.city.local.repositories;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.likandr.weather20.data.city.local.entities.CityEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface CityDao {
    @Query("SELECT * FROM city")
    Flowable<List<CityEntity>> getAll();

    @Query("DELETE FROM city")
    void deleteAll();

    @Query("SELECT id FROM city")
    Single<List<Integer>> getCityIds();

    @Query("SELECT COUNT(*) from city")
    Single<Integer> count();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<CityEntity> cityEntities);

    @Delete
    void delete(List<CityEntity> cityEntities);
}
