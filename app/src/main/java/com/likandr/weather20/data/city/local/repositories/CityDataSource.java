package com.likandr.weather20.data.city.local.repositories;

import com.likandr.weather20.data.city.local.entities.CityEntity;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

public class CityDataSource implements ICityDataSource {
    private CityDao cityDao;

    @Inject public CityDataSource(CityDao cityDao) {
        this.cityDao = cityDao;
    }

    @Override public void insert(List<CityEntity> cityEntities) {
        cityDao.insert(cityEntities);
    }

    @Override public void delete(List<CityEntity> citiesModel) {
        cityDao.delete(citiesModel);
    }

    @Override public Flowable<List<CityEntity>> getAll() {
        return cityDao.getAll();
    }
}
