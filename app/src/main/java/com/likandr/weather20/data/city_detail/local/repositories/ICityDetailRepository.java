package com.likandr.weather20.data.city_detail.local.repositories;

import com.likandr.weather20.data.city_detail.local.entities.CityDetailEntity;

import io.reactivex.Single;

public interface ICityDetailRepository {
    long insert(CityDetailEntity cityDetailEntity);

    void delete(CityDetailEntity cityDetailEntity);

    Single<CityDetailEntity> getAll();
}