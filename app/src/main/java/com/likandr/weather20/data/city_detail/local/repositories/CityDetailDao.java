package com.likandr.weather20.data.city_detail.local.repositories;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.likandr.weather20.data.city_detail.local.entities.CityDetailEntity;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface CityDetailDao {
    @Query("SELECT * FROM city_detail")
    Single<CityDetailEntity> getAll();

    @Query("DELETE FROM city_detail")
    void deleteAll();

    @Query("SELECT * FROM city_detail WHERE id=:id")
    Flowable<CityDetailEntity> findById(Integer id);

    @Query("SELECT name FROM city_detail WHERE id=:id")
    Flowable<String> getCityName(Integer id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(CityDetailEntity cityDetailEntity);

    @Delete
    void delete(CityDetailEntity cityDetailEntity);
}
