package com.likandr.weather20.data._model.converters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.likandr.weather20.data._model.Main;

import java.lang.reflect.Type;

public class MainTypeConverter {

    private Gson gson = new Gson();
    private Type type = new TypeToken<Main>(){}.getType();

    @TypeConverter public Main fromString(String json) {
        return gson.fromJson(json, type);
    }

    @TypeConverter public String toString(Main cityEntities) {
        return gson.toJson(cityEntities, type);
    }
}