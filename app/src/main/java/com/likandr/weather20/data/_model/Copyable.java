package com.likandr.weather20.data._model;

public interface Copyable<T extends Copyable<T>> {
    T getCopy();
}
