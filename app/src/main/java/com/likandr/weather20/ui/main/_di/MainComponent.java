package com.likandr.weather20.ui.main._di;

import com.likandr.weather20.common._di.AppComponent;
import com.likandr.weather20.data.city.CityRepository;
import com.likandr.weather20.data.city_detail.CityDetailRepository;
import com.likandr.weather20.ui.main.ActivityScope;
import com.likandr.weather20.ui.main.cities.CitiesPresenter;
import com.likandr.weather20.ui.main.cities.view.CitiesFragment;
import com.likandr.weather20.ui.main.details.DetailsPresenter;
import com.likandr.weather20.ui.main.details.view.DetailsFragment;

import dagger.Component;

@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = MainModule.class
)
public interface MainComponent {

    //Fragments
    void inject(CitiesFragment view);
    void inject(DetailsFragment view);

    // Presenters
    void inject(CitiesPresenter presenter);
    void inject(DetailsPresenter presenter);

    // Repositories
    void inject(CityRepository repository);
    void inject(CityDetailRepository repository);
}