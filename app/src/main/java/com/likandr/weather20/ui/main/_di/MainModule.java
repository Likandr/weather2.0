package com.likandr.weather20.ui.main._di;

import android.content.SharedPreferences;
import android.support.v4.app.Fragment;

import com.likandr.weather20.common.db.MyDatabase;
import com.likandr.weather20.data.city.CityRepository;
import com.likandr.weather20.data.city.local.repositories.CityDao;
import com.likandr.weather20.data.city.local.repositories.CityDataSource;
import com.likandr.weather20.data.city_detail.CityDetailRepository;
import com.likandr.weather20.data.city_detail.local.repositories.CityDetailDao;
import com.likandr.weather20.data.city_detail.local.repositories.CityDetailDataSource;
import com.likandr.weather20.ui.main.cities.CitiesPresenter;
import com.likandr.weather20.ui.main.details.DetailsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {
    private Fragment fragment;

    public MainModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    Fragment provideFragment() {
        return fragment;
    }

    //region presenters
    @Provides
    CitiesPresenter provideCatalogPresenter(CityRepository cityRepository) {
        return new CitiesPresenter(cityRepository);
    }

    @Provides
    DetailsPresenter provideChangePresenter(SharedPreferences sharedPreferences,
                                            CityDetailRepository cityDetailRepository) {
        return new DetailsPresenter(sharedPreferences, cityDetailRepository);
    }
    //endregion

    //region db
    @Provides
    CityDao providesCityDao(MyDatabase database) {
        return database.getCityDao();
    }

    @Provides
    CityDataSource providesCityDataSource(CityDao cityDao) {
        return new CityDataSource(cityDao);
    }

    @Provides
    CityDetailDao providesCityDetailDao(MyDatabase database) {
        return database.getCityDetailDao();
    }

    @Provides
    CityDetailDataSource providesCityDetailDataSource(CityDetailDao cityDetailDao) {
        return new CityDetailDataSource(cityDetailDao);
    }
    //endregion
}