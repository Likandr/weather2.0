package com.likandr.weather20.ui.main.details.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.cremy.greenrobotutils.library.ui.SnackBarUtils;
import com.likandr.weather20.R;
import com.likandr.weather20.common.App;
import com.likandr.weather20.common.base.BaseActivity;
import com.likandr.weather20.common.base.BaseFragment;
import com.likandr.weather20.common.base.Layout;
import com.likandr.weather20.common.base.misc.helpers.SharedPreferencesHelper;
import com.likandr.weather20.data.city_detail.local.entities.CityDetailEntity;
import com.likandr.weather20.ui.main._di.DaggerMainComponent;
import com.likandr.weather20.ui.main._di.MainComponent;
import com.likandr.weather20.ui.main._di.MainModule;
import com.likandr.weather20.ui.main.details.DetailsMVP;
import com.likandr.weather20.ui.main.details.DetailsPresenter;

import javax.inject.Inject;

import butterknife.BindView;

@Layout(id = R.layout.fragment_details)
public class DetailsFragment extends BaseFragment implements DetailsMVP.View {

    public static final String TAG = DetailsFragment.class.getSimpleName();

    @BindView(R.id.root_view)
    CoordinatorLayout rootView;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    MainComponent component;
    @Inject SharedPreferences mSharedPreferences;
    @Inject DetailsPresenter presenter;

    Toolbar mToolbar;
    MenuItem mCntMenuItem, mLangMenuItem;

    DetailsAdapter mAdapter;
    OnChangeFragment onChangeFragment;

    private int mId = 0;
    private String mCityName = "None";
    private boolean mLangEN = true;
    private boolean mCntDaysThree = true;
    private String mNameOfCity = "";

    @Override
    public void injectDependencies() {
        Log.d(TAG, "injectDependencies");
        if (component == null) {
            component = DaggerMainComponent.builder()
                    .appComponent(App.get().getComponent())
                    .mainModule(new MainModule(this))
                    .build();
            component.inject(this);
        }
    }

    @Override
    public void attachToPresenter() {
        Log.d(TAG, "attachToPresenter");
        this.presenter.attachView(this);
    }

    @Override
    public void detachFromPresenter() {
        Log.d(TAG, "detachFromPresenter");
        this.presenter.detachView();
    }

    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance() {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        this.getArgs(savedInstanceState);
        this.setUpToolbar();
    }

    @Override public void setUpToolbar() {
        setHasOptionsMenu(true);

        mToolbar = getActivity().findViewById(R.id.toolbar);
        BaseActivity activity = (BaseActivity) getActivity();
        activity.setSupportActionBar(mToolbar);

        mToolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_lang:
                    onLangChange();
                    return true;
                case R.id.action_days:
                    onDaysChange();
                    return true;
            }
            return false;
        });

        getCityData();

        ActionBar actionBar = activity.getSupportActionBar();
        if(actionBar!= null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            mToolbar.setNavigationOnClickListener(v -> activity.onBackPressed());
            actionBar.setTitle(mCityName);
        }
    }

    private void getCityData() {
        mId = SharedPreferencesHelper.getInteger(
                mSharedPreferences, SharedPreferencesHelper.KEY_CURRENT_CITY_ID, 0);

        mCityName = SharedPreferencesHelper.getString(
                mSharedPreferences, SharedPreferencesHelper.KEY_CURRENT_CITY_NAME);

        mLangEN = SharedPreferencesHelper.getBoolean(
                mSharedPreferences, SharedPreferencesHelper.KEY_CURRENT_LANG);

        mCntDaysThree = SharedPreferencesHelper.getBoolean(
                mSharedPreferences, SharedPreferencesHelper.KEY_CURRENT_DAYS_SHOW);
    }

    private void onLangChange() {
        mLangEN = !mLangEN;
        SharedPreferencesHelper.putBoolean(
                mSharedPreferences, SharedPreferencesHelper.KEY_CURRENT_LANG, mLangEN);
        setLangIcon();
        refreshDataList();
    }

    private void setLangIcon() {
        if (mLangMenuItem != null) {
            mLangMenuItem.setIcon(mLangEN ? R.drawable.ic_flag_en : R.drawable.ic_flag_ru);
        }
    }

    private void onDaysChange() {
        mCntDaysThree = !mCntDaysThree;
        SharedPreferencesHelper.putBoolean(
                mSharedPreferences, SharedPreferencesHelper.KEY_CURRENT_DAYS_SHOW, mCntDaysThree);
        setDaysIcon();
        refreshDataList();
    }

    private void setDaysIcon() {
        if (mCntMenuItem != null) {
            mCntMenuItem.setIcon(mCntDaysThree ? R.drawable.ic_action_three : R.drawable.ic_action_seven);
        }
    }

    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);

        onSetupMenu(menu);
    }

    private void onSetupMenu(Menu menu) {
        mLangMenuItem = menu.findItem(R.id.action_lang);
        mLangMenuItem.setVisible(true);
        mCntMenuItem = menu.findItem(R.id.action_days);
        mCntMenuItem.setVisible(true);

        setLangIcon();
        setDaysIcon();
    }

    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        this.initRecyclerView();

        refreshDataList();
    }

    private void refreshDataList() {
        presenter.loadData(mId, mLangEN, mCntDaysThree);
    }

    @Override public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        this.injectDependencies();
        this.attachToPresenter();
        super.onAttach(context);
        if (context instanceof OnChangeFragment) {
            onChangeFragment = (OnChangeFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangeFragment");
        }
    }

    @Override public void onDetach() {
        Log.d(TAG, "onDetach");
        detachFromPresenter();
        super.onDetach();
    }

    @Override public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
    }

    public void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override public void showData(CityDetailEntity cityDetailEntity) {
        if (mAdapter != null) {
            mAdapter.setItems(cityDetailEntity.getList());
        } else {
            mAdapter = new DetailsAdapter(getFragmentActivity(), cityDetailEntity.getList());
        }
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override public void showGalleryError() {

    }

    @Override public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override public void showMessage(String message) {
        SnackBarUtils.showSimpleSnackbar(rootView, message);
    }

    @Override public void showNoNetwork() {

    }

    @Override public void getArgs(Bundle _bundle) {

    }
}