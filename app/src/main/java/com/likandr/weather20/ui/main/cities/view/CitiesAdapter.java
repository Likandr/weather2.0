package com.likandr.weather20.ui.main.cities.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.likandr.weather20.R;
import com.likandr.weather20.data.city.local.entities.CityEntity;

import java.util.List;

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {

    private List<CityEntity> mCities;

    private final LayoutInflater inflater;
    @Nullable
    private OnClickListener mOnClickListener;

    public CitiesAdapter(Context context, List<CityEntity> cityModels) {
        this.mCities = cityModels;
        inflater = LayoutInflater.from(context);
    }

    public void setItems(List<CityEntity> cityModels) {
        this.mCities = cityModels;
        notifyDataSetChanged();
    }

    public void setOnClickListener(@Nullable OnClickListener onClickListener) {
        this.mOnClickListener = onClickListener;
    }

    @Override @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CityItem view = (CityItem) inflater.inflate(R.layout.city_item, parent, false);
        return new ViewHolder(view);
    }

    @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mCities.get(position));
        holder.itemView.setOnClickListener(v -> {
            if (mOnClickListener != null) {
                mOnClickListener.onItemClicked(mCities.get(position));
            }
        });
    }

    @Override public int getItemCount() {
        return mCities.size();
    }

    public static final class ViewHolder extends RecyclerView.ViewHolder {
        private ViewHolder(CityItem cityItem) {
            super(cityItem);
        }

        private void bindTo(CityEntity entry) {
            ((CityItem) itemView).bindTo(entry);
        }
    }

    public interface OnClickListener {
        void onItemClicked(CityEntity cityEntity);
    }
}