package com.likandr.weather20.ui.main.cities;

import android.support.annotation.NonNull;

import com.likandr.weather20.common.base.misc.Params;
import com.likandr.weather20.common.base.misc.Utils;
import com.likandr.weather20.common.base.misc.rx.RxUtils;
import com.likandr.weather20.common.base.presenter.BasePresenter;
import com.likandr.weather20.data.city.CityRepository;
import com.likandr.weather20.data.city.local.entities.CityEntity;
import com.likandr.weather20.data.city.remote.interactors.GetCityUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;

public class CitiesPresenter extends BasePresenter<CitiesMVP.View> implements CitiesMVP.Presenter {
    private final static String TAG = CitiesPresenter.class.getName();

    private final CityRepository cityRepository;
    private Disposable d1, d2;

    @Inject public CitiesPresenter(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override public void attachView(CitiesMVP.View view) {
        super.attachView(view);
    }

    @Override public void detachView() {
        super.detachView();
        if (d1 != null && !d1.isDisposed()) {
            d1.dispose();
        }

        if (d2 != null && !d2.isDisposed()) {
            d2.dispose();
        }
    }

    //region runUp
    @Override @SuppressWarnings("unchecked")
    public void runUp() {
        view.showLoading();

        d1 = RxUtils.performRequestSingle(
                cityRepository.getCityIds(),
                this::runupOK,
                this::runupBAD);
    }

    @Override public void runupOK(@NonNull List<Integer> list) {
        checkViewAttached();

        Params params = Params.create();
        if (list.size() != 0) {
            params.putString(GetCityUseCase.PARAMS_KEY_IDS_OF_CITY, convertToString(list));
        }
        params.putString(GetCityUseCase.PARAMS_KEY_LANGUAGE, view.getLang() ? "en" : "ru");

        loadData(params);
    }

    private String convertToString(List<Integer> numbers) {
        StringBuilder builder = new StringBuilder();

        for (int number : numbers) {
            builder.append(number);
            builder.append(",");
        }
        builder.setLength(builder.length() - 1);
        return builder.toString();
    }

    @Override public void runupBAD(Throwable e) {
        e.printStackTrace();
        checkViewAttached();
        view.hideLoading();
        view.showDataError();
    }
    //endregion

    //region loadData
    @Override public void loadData(Params params) {
        if (Utils.isNetworkAvailable(view.getContext()))
            addDisposableLoadData(cityRepository.getCityList(params));
        else {
            view.showMessage("network not available");
            addDisposableLoadData(cityRepository.getCityListNoConnect());
        }
    }

    private void addDisposableLoadData(Flowable<List<CityEntity>> flowable) {
        d2 = RxUtils.performRequestFlowable(
                flowable,
                this::onLoadDataSuccess,
                this::onLoadDataFailure);
    }

    @Override public void onLoadDataSuccess(List<CityEntity> cityEntities) {
        checkViewAttached();
        view.hideLoading();
        view.showData(cityEntities);
    }

    @Override public void onLoadDataFailure(Throwable e) {
        e.printStackTrace();
        checkViewAttached();
        view.hideLoading();
        view.showDataError();
    }
    //endregion
}
