package com.likandr.weather20.ui.main;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.os.Bundle;
import android.view.View;

import com.likandr.weather20.R;
import com.likandr.weather20.common.base.BaseActivity;
import com.likandr.weather20.common.base.BaseFragment;
import com.likandr.weather20.common.base.Layout;
import com.likandr.weather20.ui.main.cities.view.CitiesFragment;
import com.likandr.weather20.ui.main.details.view.DetailsFragment;

@Layout(id = R.layout.activity_main)
public class MainActivity extends BaseActivity implements BaseFragment.OnChangeFragment {
    private String currentFragmentTag;

    public static void startMe(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getExtras(getIntent());
        this.setUpToolbar();

        initFragment(CitiesFragment.TAG);
    }

    private void initFragment(String fragmentTag) {
        currentFragmentTag = fragmentTag;
        Fragment fragment = getFragment(fragmentTag);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, getFragment(fragmentTag));

        fragmentTransaction.replace(R.id.content_frame, fragment).commit();
    }

    private Fragment getFragment(String fragmentTag) {

        if (fragmentTag.equals(DetailsFragment.TAG)) {
            return DetailsFragment.newInstance();
        }

        return CitiesFragment.newInstance();
    }

    @Override
    public void getExtras(Intent _intent) {

    }

    @Override
    public void closeActivity() {
        this.finish();
    }

    @Override
    public void setUpToolbar() {
        setTitle(null);
    }

    @Override
    public Fragment getAttachedFragment(int id) {
        return getSupportFragmentManager().findFragmentById(id);
    }

    @Override
    public Fragment getAttachedFragment(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    @Override
    public void onChangeFragment(String fragmentTag,
                                 @Nullable Pair<View, String> sharedElement) {
        initFragment(fragmentTag);
    }

    @Override
    public void onBackPressed() {
        if (!currentFragmentTag.equals(CitiesFragment.TAG)) {
            initFragment(CitiesFragment.TAG);
        } else {
            finish();
        }
    }
}