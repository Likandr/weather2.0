package com.likandr.weather20.ui.main.cities.view;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.TextView;

import com.likandr.weather20.R;
import com.likandr.weather20.data.city.local.entities.CityEntity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CityItem extends CardView {

    @BindView(R.id.city_name) TextView title;
    @BindView(R.id.city_temp) TextView date;

    public CityItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void bindTo(final CityEntity item) {
        title.setText(item.getName());
        date.setText(String.valueOf(item.getMain().getTemp()));
    }
}