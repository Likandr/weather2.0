package com.likandr.weather20.ui.main.details;

import android.content.SharedPreferences;

import com.likandr.weather20.common.base.misc.Params;
import com.likandr.weather20.common.base.misc.helpers.SharedPreferencesHelper;
import com.likandr.weather20.common.base.misc.rx.RxUtils;
import com.likandr.weather20.common.base.presenter.BasePresenter;
import com.likandr.weather20.data.city_detail.CityDetailRepository;
import com.likandr.weather20.data.city_detail.local.entities.CityDetailEntity;
import com.likandr.weather20.data.city_detail.remote.interactors.GetDetailsUseCase;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class DetailsPresenter extends BasePresenter<DetailsMVP.View> implements DetailsMVP.Presenter {

    private final static String TAG = DetailsPresenter.class.getName();

    private final SharedPreferences mSharedPreferences;
    private final CityDetailRepository mRepository;
    private Disposable d1;

    @Inject public DetailsPresenter(SharedPreferences sharedPreferences,
                                    CityDetailRepository cityDetailRepository) {
        this.mSharedPreferences = sharedPreferences;
        this.mRepository = cityDetailRepository;
    }

    @Override public void attachView(DetailsMVP.View view) {
        super.attachView(view);
    }

    @Override public void detachView() {
        super.detachView();
        if (d1 != null && !d1.isDisposed()) {
            d1.dispose();
        }
    }

    //region loadData
    @Override public void loadData(int id, boolean langEN, boolean cntDaysThree) {
        view.showLoading();

        d1 = RxUtils.performRequestFlowable(
                mRepository.getCityDetail(getParams(langEN, cntDaysThree), getId()),
                this::onLoadDataSuccess,
                this::onLoadDataFailure);
    }

    private Params getParams(boolean langEN, boolean cntDaysThree) {
        Params params = Params.create();
        params.putInt(GetDetailsUseCase.PARAMS_KEY_IDS_OF_CITY, getId());
        params.putInt(GetDetailsUseCase.PARAMS_KEY_COUNT_DAYS, cntDaysThree ? 3 : 7);
        params.putString(GetDetailsUseCase.PARAMS_KEY_LANGUAGE, langEN ? "en" : "ru");
        return params;
    }

    private int getId() {
        return SharedPreferencesHelper.getInteger(
                mSharedPreferences, SharedPreferencesHelper.KEY_CURRENT_CITY_ID, 0);
    }

    @Override public void onLoadDataSuccess(CityDetailEntity cityDetailEntity) {
        checkViewAttached();
        view.hideLoading();
        view.showData(cityDetailEntity);
    }

    @Override public void onLoadDataFailure(Throwable e) {
        e.printStackTrace();
        checkViewAttached();
        view.hideLoading();
        view.showGalleryError();
    }
    //endregion
}