package com.likandr.weather20.ui.main.details.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.likandr.weather20.R;
import com.likandr.weather20.data._model.DailyForecast;

import java.util.List;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.ViewHolder> {

    private List<DailyForecast> mForecast;

    private final LayoutInflater inflater;

    public DetailsAdapter(Context context, List<DailyForecast> dailyForecasts) {
        this.mForecast = dailyForecasts;
        inflater = LayoutInflater.from(context);
    }

    public void setItems(List<DailyForecast> dailyForecasts) {
        this.mForecast = dailyForecasts;
        notifyDataSetChanged();
    }

    @Override @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DetailsItem view = (DetailsItem) inflater.inflate(R.layout.deteils_item, parent, false);
        return new ViewHolder(view);
    }

    @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mForecast.get(position));
    }

    @Override public int getItemCount() {
        return mForecast.size();
    }

    public static final class ViewHolder extends RecyclerView.ViewHolder {
        private ViewHolder(DetailsItem detailsItem) {
            super(detailsItem);
        }

        private void bindTo(DailyForecast dailyForecast) {
            ((DetailsItem) itemView).bindTo(dailyForecast);
        }
    }
}