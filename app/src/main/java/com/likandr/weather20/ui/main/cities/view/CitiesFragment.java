package com.likandr.weather20.ui.main.cities.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.cremy.greenrobotutils.library.ui.SnackBarUtils;
import com.likandr.weather20.R;
import com.likandr.weather20.common.App;
import com.likandr.weather20.common.base.BaseActivity;
import com.likandr.weather20.common.base.BaseFragment;
import com.likandr.weather20.common.base.Layout;
import com.likandr.weather20.common.base.misc.helpers.SharedPreferencesHelper;
import com.likandr.weather20.data.city.local.entities.CityEntity;
import com.likandr.weather20.ui.main._di.DaggerMainComponent;
import com.likandr.weather20.ui.main._di.MainComponent;
import com.likandr.weather20.ui.main._di.MainModule;
import com.likandr.weather20.ui.main.cities.CitiesMVP;
import com.likandr.weather20.ui.main.cities.CitiesPresenter;
import com.likandr.weather20.ui.main.details.view.DetailsFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

@Layout(id = R.layout.fragment_cities)
public class CitiesFragment extends BaseFragment implements CitiesMVP.View, CitiesAdapter.OnClickListener {

    public static final String TAG = CitiesFragment.class.getSimpleName();

    @BindView(R.id.root_view)
    CoordinatorLayout rootView;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.cta_create)
    FloatingActionButton ctaCreate;
    @Nullable @BindView(R.id.container_placeholder)
    FrameLayout containerPlaceholder;

    MainComponent component;
    @Inject SharedPreferences mSharedPreferences;
    @Inject CitiesPresenter presenter;

    Toolbar mToolbar;
    MenuItem mLangMenuItem;

    CitiesAdapter mAdapter;
    OnShowMessageListener onShowMessageListener;
    OnChangeFragment onChangeFragment;

    boolean langEN = true;

    @Override
    public void injectDependencies() {
        Log.d(TAG, "injectDependencies");
        if (component == null) {
            component = DaggerMainComponent.builder()
                    .appComponent(App.get().getComponent())
                    .mainModule(new MainModule(this))
                    .build();
            component.inject(this);
        }
    }

    @Override
    public void attachToPresenter() {
        Log.d(TAG, "attachToPresenter");
        this.presenter.attachView(this);
    }

    @Override
    public void detachFromPresenter() {
        Log.d(TAG, "detachFromPresenter");
        this.presenter.detachView();
    }

    public CitiesFragment() {
        // Required empty public constructor
    }

    public static CitiesFragment newInstance() {
        return new CitiesFragment();
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        this.getArgs(savedInstanceState);
        this.setUpToolbar();
    }

    @Override public void setUpToolbar() {
        setHasOptionsMenu(true);

        mToolbar = getActivity().findViewById(R.id.toolbar);
        BaseActivity activity = (BaseActivity) getActivity();
        activity.setSupportActionBar(mToolbar);

        mToolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_lang:
                    onLangChange();
                    return true;
            }
            return false;
        });

        ActionBar actionBar = activity.getSupportActionBar();
        if(actionBar!= null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setTitle("Weather 2.0");
        }
    }

    private void onLangChange() {
        langEN = !langEN;
        SharedPreferencesHelper.putBoolean(
                mSharedPreferences, SharedPreferencesHelper.KEY_CURRENT_LANG, langEN);
        mLangMenuItem.setIcon(langEN ? R.drawable.ic_flag_en : R.drawable.ic_flag_ru);

        presenter.runUp();
    }

    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);

        onSetupMenu(menu);
    }

    private void onSetupMenu(Menu menu) {
        mLangMenuItem = menu.findItem(R.id.action_lang);
        mLangMenuItem.setVisible(true);
    }

    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        this.initRecyclerView();

        presenter.runUp();
    }

    @Override public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        this.injectDependencies();
        this.attachToPresenter();
        super.onAttach(context);
        if (context instanceof OnChangeFragment) {
            onChangeFragment = (OnChangeFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangeFragment");
        }
    }

    @Override public void onDetach() {
        Log.d(TAG, "onDetach");
        detachFromPresenter();
        super.onDetach();
    }

    @Override public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
    }

    public void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        SnackBarUtils.showSimpleSnackbar(rootView, message);
    }

    @Override
    public void showData(List<CityEntity> cityModel) {
        setViewStub(false);

        if (mAdapter != null) {
            mAdapter.setItems(cityModel);
        } else {
            mAdapter = new CitiesAdapter(getFragmentActivity(), cityModel);
            mAdapter.setOnClickListener(this);
        }
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setViewStub(boolean b) {
        if (b) {
            //if (mLangMenuItem != null) mLangMenuItem.setVisible(false);

            mRecyclerView.setVisibility(View.GONE);
            if (containerPlaceholder == null && getView() != null) {
                containerPlaceholder = (FrameLayout) ((ViewStub) getView().findViewById(R.id.viewstub_city)).inflate();
            }
            containerPlaceholder.setVisibility(View.VISIBLE);
        } else {
            //if (mLangMenuItem != null) mLangMenuItem.setVisible(true);

            mRecyclerView.setVisibility(View.VISIBLE);
            if (containerPlaceholder != null) {
                containerPlaceholder.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void showNoNetwork() {

    }

    @Override
    public void showDataError() {

    }

    @Override
    public void onItemClicked(CityEntity cityEntity) {
        SharedPreferencesHelper.putInteger(
                mSharedPreferences, SharedPreferencesHelper.KEY_CURRENT_CITY_ID, cityEntity.getId());
        SharedPreferencesHelper.putString(
                mSharedPreferences, SharedPreferencesHelper.KEY_CURRENT_CITY_NAME, cityEntity.getName());
        onChangeFragment.onChangeFragment(DetailsFragment.TAG, null);
    }

    @Override
    public void getArgs(Bundle _bundle) {

    }

    @Override public boolean getLang() {
        return langEN;
    }
}