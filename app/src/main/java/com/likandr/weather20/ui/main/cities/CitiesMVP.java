package com.likandr.weather20.ui.main.cities;

import com.likandr.weather20.common.base.BaseMvpView;
import com.likandr.weather20.common.base.misc.Params;
import com.likandr.weather20.data.city.local.entities.CityEntity;

import java.util.List;

public class CitiesMVP {

    public interface View extends BaseMvpView {
        void showData(List<CityEntity> cityModels);
        void showDataError();
        boolean getLang();
    }

    interface Presenter {
        void runUp();
        void runupOK(List<Integer> list);
        void runupBAD(Throwable e);

        void loadData(Params params);
        void onLoadDataSuccess(List<CityEntity> citiesModel);
        void onLoadDataFailure(Throwable e);
    }
}