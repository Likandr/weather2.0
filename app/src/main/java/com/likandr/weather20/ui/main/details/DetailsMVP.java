package com.likandr.weather20.ui.main.details;

import com.likandr.weather20.common.base.BaseMvpView;
import com.likandr.weather20.data.city_detail.local.entities.CityDetailEntity;

public class DetailsMVP {
    public interface View extends BaseMvpView {
        void showData(CityDetailEntity cityDetailEntity);
        void showGalleryError();
    }

    interface Presenter {
        void loadData(int id, boolean langEN, boolean cntDaysThree);

        void onLoadDataSuccess(CityDetailEntity cityDetailEntity);
        void onLoadDataFailure(Throwable e);
    }
}