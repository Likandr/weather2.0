package com.likandr.weather20.ui.main.details.view;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.TextView;

import com.likandr.weather20.R;
import com.likandr.weather20.common.base.misc.Utils;
import com.likandr.weather20.data._model.DailyForecast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsItem extends CardView {

    @BindView(R.id.city_temp) TextView temp;
    @BindView(R.id.city_clouds) TextView clouds;
    @BindView(R.id.city_date) TextView date;

    public DetailsItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void bindTo(final DailyForecast item) {
        temp.setText(String.valueOf(item.getTemp().getDay()));
        clouds.setText(String.valueOf(item.getWeather().get(0).getDescription()));
        date.setText(Utils.getDate(item.getDt()));
    }
}